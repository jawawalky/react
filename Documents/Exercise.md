# Function Components

## What is a Function Component?
In *React* a UI component can be implemented as a function. The function
returns a *JSX* expression, which contains the code that renders the component
UI.

The general structure of a function component looks like this

```
function AddressDetails(props) {
    return (
        <div>
            <input type="text" value={props.address.street}/>
            <input type="text" value={props.address.zipCode}/>
            <input type="text" value={props.address.city}/>
        </div>
    );
}
```

The function returns the *HTML* to be rendered. `props` are the parameters
or properties passed as input to the component. A component can be embedded
into another component by adding a pseudo-*HTML* element to the *HTML* UI of
the parent component.

```
function AddressBook(props) {
    const homeAddress = new Address(...);
    const workAddress = new Address(...);
    return (
        <>
            <div>
                Home:<br/>
                <AddressDetails address="homeAddress"/>
            </div>
            <div>
                Work:<br/>
                <AddressDetails address="workAddress"/>
            </div>
        </>
    );
}
```

Suppose the addresses of the address book are stored in an array, then
the code of the address book component could look like this

```
function AddressBook(props) {
    const addresses = props.addresses; // <- Address array passed as input
                                       //    property to the component.

    const items = addresses.map(item => <div><AddressDetails address={item}/></div>);

    return (
        <>
            {items}
        </>
    );
}
```

## What to Do?
1. We want to implement two components.
1. The first component should display a list item. For now it should only show the todo's caption.
1. Open the file [TodoListItem.jsx](../Projects/client/src/components/TodoListItem.jsx).
1. Create a functional component `TodoListItem`, which displays the caption of a todo in a text field.
1. The value of the text field should come from some variable or constant (interpolation).
1. Open the file [Clock.jsx](../Projects/client/src/components/Clock.jsx).
1. Implement a functional component `Clock`, which displays the time, when it was created.
1. Open the file [App.jsx](../Projects/client/src/App.jsx).
1. Add a `TodoListItem` and a `Clock` component to your application.
