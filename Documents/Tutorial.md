# Function Components

## What are Components?
The structure of a Web page is defined by a tree of *HTML* elements called
the *DOM* (= *Document Object Model*). Parts of that tree can be grouped
together and represented by a *React* component. So the a Web UI is composed
of a set of *React* components.

A *React* component is a piece of software, which produces *HTML* as output.
The produced *HTML* is rendered in the *DOM*. *React* components are written in
[JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Introduction)
or alternatively in [TypeScript](https://www.typescriptlang.org/).

## What is a Function Component?
*Function Components* or *Functional Components* are components written as pure
functions. The input parameters of the function is a set of *properties*,
which represents the data that can be passed from outside into the component.
The return value of the function is the *HTML* that should be rendered in
the *DOM* of the Web page.

### Pure Functions
It is important that a component function is *pure*. In this context *pure*
means that the function has no side-effects. The same input always produces
the same output.

**Example**

```
function add(a, b) { return a + b; }

console.log('A) ' + add(3, 4));
console.log('B) ' + add(3, 4));
```

**Output**

```
A) 7
B) 7
```

The function `add(...)` is pure, because calling it with the same values for
`a` and `b` will always produce the same result.

Whereas the following function would be impure

```
let c = 0;

function add(a, b) { c++; return a + b + c; }

console.log('A) ' + add(3, 4));
console.log('B) ' + add(3, 4));
```

**Output**

```
A) 8
B) 9
```

because it modifies an external variable, which leads to different results
although called with the same arguments `3` and `4`.

### Component Function
As said a function-based *React* component may have some input and produces
*HTML* output. We use *JSX* to produce the *HTML* output. So the following
function is a valid *React* component

```
export default function FlavorList() {
    return (
        <ul>
            <li>Strawberry</li>
            <li>Chocolate</li>
            <li>Lemon</li>
            <li>Mango</li>
        </ul>
    );
}
```

The name of the function defines an *HTML* pseudo-element, which we can
use is another *JSX* expression. The *HTML* returned by the component will
be integrated into the surrounding *HTML*.

Suppose we had the following component

```
export default function App() {
    return (
        <div>
            <h1>Flavors</h1>
            <ul>
                <li>Strawberry</li>
                <li>Chocolate</li>
                <li>Lemon</li>
                <li>Mango</li>
            </ul>
        </div>
    );
}
```

By using the component `FlavorList` we could rewrite the `App` component
the following way

```
export default function App() {
    return (
        <div>
            <h1>Flavors</h1>
            <FlavorList/>
        </div>
    );
}
```

> The name of the function is used as the name of the *HTML* element, which
> represents the component.

When *React* renders the component, it actually calls the component function
and replaces the component element in the surrounding *HTML* by the *HTML*
returned by the function.

So complex UIs are created by nesting *React* components.

## Properties
Properties represent the data that can be passed to a component. We can pass
anything from simple values to complex objects and even functions as
properties to a component.

Each property has

- a name and
- is read-only.

**Example**

```
export default function Flavor(props) {
    return (
        <li>{props.name} ({props.price})</li>
    );
}
```

Since properties are passed as one object, we can also specify the properties
the following way

```
export default function Flavor({ name, price }) {
    return (
        <li>{name} ({price})</li>
    );
}
```

which makes the access to the property values inside the component even easier.

When we use our `Flavor` component, then we can pass the properties as
attributes of the *HTML* element.

```
export default function FlavorList() {
    return (
        <ul>
            <Flavor name='Strawberry' price=1.50/>
            <Flavor name='Chocolate' price=2.00/>
            <Flavor name='Lemon' price=1.20/>
            <Flavor name='Mango' price=1.40/>
        </ul>
    );
}
```

### Default Values
Using the deconstructor version of the properties representation allows us
to set default values, if wanted or needed.

```
export default function Flavor({ name, price = 1.50}) {
    return (
        <li>{name} ({price})</li>
    );
}
```

So the `FlavorList` becomes

```
export default function FlavorList() {
    return (
        <ul>
            <Flavor name='Strawberry'/>
            <Flavor name='Chocolate' price=2.00/>
            <Flavor name='Lemon' price=1.20/>
            <Flavor name='Mango' price=1.40/>
        </ul>
    );
}
```
