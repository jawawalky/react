# React
## What You Will Learn
In this course you will learn

- what *React* is
- how to install *React*
- how to create, build and run *React* projects/applications
- how to write *React* Web components
- how to structure your *React* application
- about the main features of the *React* framework

## What You Need
For this course you need the following programs and tools

- [GIT](https://git-scm.com/) - your version control system
- [node.js](https://nodejs.org/) - your *JavaScript* runtime
- [Visual Sudio Code](https://code.visualstudio.com/) - your IDE
- [Chrome](https://www.google.com/chrome/download-chrome/), [Firefox](https://www.mozilla.org/en-US/firefox/), etc. - your browser

# Demos and Exercises
The demos and exercises are organized in *GIT* branches. The demo branches start with *demo/...* and the exercise branches start with *exercise/...*

## The Trail
The trail is a good way to learn things step by step. The demos of the trail can be found under *demo/trail/...* and the exercises under *exercise/trail/...*

This is the sequence, in which the trail is organized

1. *demo/trail/jsx*
1. *demo/trail/component*
1. *demo/trail/properties*
1. *demo/trail/list-rendering*
1. *demo/trail/events*
1. *demo/trail/state-hooks*
1. *demo/trail/conditional-rendering*
1. *demo/trail/lifting-up-state*
1. *demo/trail/forms*
1. *demo/trail/reducer*
1. *demo/trail/context*
1. *demo/trail/async*
1. *demo/trail/effects*
1. *demo/trail/routing*
1. *demo/trail/login*
1. *demo/trail/fetch*

# Software
The following third party software has been used

## React
The *React* framework is a *JavaScript*-based Web framework for reactive
Web applications. It is published under
[MIT license](https://github.com/facebook/react/blob/main/LICENSE).

## Water.css
[Water.css](https://github.com/kognise/water.css) is a drop-in *CSS* framework
for pretty layout of pure *HTML*. It is published under
[MIT license](https://github.com/kognise/water.css/blob/master/LICENSE.md).

## React Icons
[React Icons](https://react-icons.github.io/react-icons/)
is a library with a set of icons. It is published under
[Apache license](https://github.com/google/material-design-icons/blob/master/LICENSE).
