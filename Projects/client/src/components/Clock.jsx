import React from 'react';

export default function Clock() {
    const now = new Date();
    return (
        <div>
            {now.toLocaleTimeString()}
        </div>
    );
}