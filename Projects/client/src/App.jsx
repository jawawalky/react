import React from 'react';
import './App.css';
import Clock from './components/Clock';
import TodoListItem from './components/TodoListItem';

function App() {

  return (
    <>
      <header>
        <h1>React</h1>
        <h3>A Web Framework</h3>
      </header>

      <main>
        <div>
          Todo: <p><TodoListItem></TodoListItem></p>
        </div>
        <div>
          Time: <Clock></Clock>
        </div>
      </main>

      <footer>
        <p>
          &copy; Copyright - 2023 by jukia.com.<br/>
          All rights reversed.
        </p>
      </footer>
    </>
  );
}

export default App;
